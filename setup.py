#!/usr/bin/env python3


from setuptools import setup


def readme():
    with open('README.md', encoding='utf-8') as f:
        return f.read()


setup(
    name='qrfetch',
    version='1.0',
    description='System info in QR code',
    author='XGQT',
    author_email='xgqt@riseup.net',
    url='https://gitlab.com/xgqt/qrfetch',
    long_description=readme(),
    long_description_content_type='text/markdown',
    license='ISC',
    keywords='qr system',
    python_requires=">=3.5.*",
    install_requires=['qrcode'],
    scripts=['qrfetch'],
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        'Development Status :: Beta',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: Python Software Foundation License',
        'Operating System :: POSIX',
        'Programming Language :: Python',
        'Topic :: Communications :: Email',
        'Topic :: Software Development :: Bug Tracking',
    ]
)
